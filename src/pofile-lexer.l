/* -*- Mode: c; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*-
 *
 * pofile-lexer.l: Lexical analyser for po-files.
 *
 * Copyright (C) 2000 Andreas Persenius
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Andreas Persenius <ndap@swipnet.se>
 */
%{
#include "pofile-parser.h"
#include <glib.h>
%}

%option noyywrap
%x XSTRING
ws	[ \t\n\0]

%%

#[^\n]*        { yylval.str = yytext; return(COMMENT); }
"msgid"	         return(MSGID);
"msgstr"         return(MSGSTR);

<INITIAL>{ws}	 /* skip whitespace */ ;

<INITIAL>\"      BEGIN(XSTRING);
<XSTRING>\\.     yymore();
<XSTRING>[^\"\n] yymore();
<XSTRING>[\"\n]  {
    unput('\0');
    yylval.str = g_strdup(yytext);
    BEGIN(0);
    return(STRING);
}

%%

/* A hollow, empty void of nothingness */
