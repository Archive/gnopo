/* -*- Mode: c; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*-
 *
 * pofile-parser.h: Parsing functions.
 *
 * Copyright (C) 2000 Andreas Persenius
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Andreas Persenius <ndap@swipnet.se>
 */
#ifndef __POFILE_PARSER_H__
#define __POFILE_PARSER_H__

#include <glib.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

GSList *pofile_parser_parse_file(FILE *fp);
char *pofile_parser_get_obsolete(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __POFILE_PARSER_H__ */
