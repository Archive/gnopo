/* -*- Mode: c; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*-
 *
 * gnopo-preferences.c: Builds the preferences dialog for Gnopo.
 *
 * Copyright (C) 2000 Andreas Persenius
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Andreas Persenius <ndap@swipnet.se>
 */
#include "gnopo-prefs-dialog.h"
#include "gnopo-preferences.h"
#include <gnome.h>
#include <gconf/gconf-client.h>

enum {
	ENTRY_NAME = 0,
	ENTRY_EMAIL = 1
};

enum {
	COLOR_TRANSLATED,
	COLOR_FUZZY,
	COLOR_UNTRANSLATED
};

static void prefwindow_destroy_cb(GtkWidget *win, gpointer data);
static GtkWidget *build_index_view_page(GConfClient *client);
static void index_view_mitem_activate_cb(GtkWidget *mitem, gpointer data);
static GtkWidget *build_header_page(GConfClient *client);
static GtkWidget *build_general_settings_page(GConfClient *client);
static GtkWidget *build_textbox_page(GConfClient *client);

static GtkWidget *prefwindow = NULL;

void
gnopo_config_show_prefs_dialog(GtkWindow *parent)
{
	GtkWidget *page;
	GConfClient *gconf_client;
	
	g_assert(parent != NULL);

	if (prefwindow) {
		return;
	}

	gconf_client = gconf_client_get_default();

	prefwindow = gnopo_prefs_dialog_new(_("Gnopo Preferences"));
	gnome_dialog_set_parent(GNOME_DIALOG(prefwindow), parent);
	gtk_signal_connect(GTK_OBJECT(prefwindow), "destroy",
			   prefwindow_destroy_cb, NULL);

	page = build_index_view_page(gconf_client);
	gnopo_prefs_dialog_add_page(GNOPO_PREFS_DIALOG(prefwindow),
				    page, _("Message list"));

	page = build_header_page(gconf_client);
	gnopo_prefs_dialog_add_page(GNOPO_PREFS_DIALOG(prefwindow),
				     page, _("File header"));

	page = build_general_settings_page(gconf_client);
	gnopo_prefs_dialog_add_page(GNOPO_PREFS_DIALOG(prefwindow),
				    page, _("General settings"));

	page = build_textbox_page(gconf_client);
	gnopo_prefs_dialog_add_page(GNOPO_PREFS_DIALOG(prefwindow),
				    page, _("Text editing"));
	
	gtk_widget_show(prefwindow);
}

static void
prefwindow_destroy_cb(GtkWidget *win, gpointer data)
{
	prefwindow = NULL;
}

static void
index_view_pref_set_color_picker_color(GtkWidget *cp, const char *gconf_key)
{
	GConfClient *client;
	GSList *color_list;
	int color[3];
	int i;

	g_return_if_fail(cp != NULL);
	g_return_if_fail(gconf_key != NULL);

	client = gconf_client_get_default();

	color_list = gconf_client_get_list(client, gconf_key,
					   GCONF_VALUE_INT, NULL);
	if (!color_list) {
		return;
	}
	
	for (i = 0; i < 3; ++i) {
		color[i] = GPOINTER_TO_INT(color_list->data);
		color_list = g_slist_next(color_list);
	}
	gnome_color_picker_set_i16(GNOME_COLOR_PICKER(cp),
				   (gushort) color[0],
				   (gushort) color[1],
				   (gushort) color[2],
				   (gushort) 65535);
	
	g_assert(g_slist_next(color_list) == NULL);
}

static void
index_view_color_picker_color_set_cb(GtkWidget *colorpicker,
				     guint r, guint g, guint b, guint a,
				     gpointer data)
{
	GConfClient *client;
	GError *error = NULL;
	GSList *colorvalues = NULL;
	int colortype;
	char *key = NULL;

	colortype = GPOINTER_TO_INT(data);
	client = gconf_client_get_default();

	colorvalues = g_slist_append(colorvalues, (gpointer) ((int) r));
	colorvalues = g_slist_append(colorvalues, (gpointer) ((int) g));
	colorvalues = g_slist_append(colorvalues, (gpointer) ((int) b));

	if (colortype == COLOR_TRANSLATED) {
		key = "/apps/gnopo/index_view/translated_color";
	} else if (colortype == COLOR_FUZZY) {
		key = "/apps/gnopo/index_view/fuzzy_color";
	} else if (colortype == COLOR_UNTRANSLATED) {
		key = "/apps/gnopo/index_view/untranslated_color";
	} else {
		g_print("colortype %i not in range.. aborting\n", colortype);
		g_assert_not_reached();
	}

	gconf_client_set_list(client, key, GCONF_VALUE_INT,
			      colorvalues, &error);

	if (error != NULL) {
		g_message("Error setting key \"%s\": %s\n", key,
			  error->message);
		g_error_free(error);
		return;
	}
	gconf_client_suggest_sync(client, NULL);
}

static GtkWidget *
build_index_view_page(GConfClient *client)
{
	GtkWidget *main_vbox;
	GtkWidget *frame;
	GtkWidget *hbox;
	GtkWidget *label;
	GtkWidget *option_menu, *menu, *mitem;
	GtkWidget *table, *color_picker[3];
	static char *mitem_texts[] = {
		N_("Show all"),
		N_("Show fuzzy"),
		N_("Show untranslated"),
		N_("Show fuzzy & untranslated"),
		NULL
	};
	static char *color_texts[] = {
		N_("Translated messages:"),
		N_("Fuzzy messages:"),
		N_("Untranslated messages:"),
		NULL
	};
	int i;

	main_vbox = gtk_vbox_new(FALSE, 4);
	
	/* Mode frame */

	frame = gtk_frame_new(_("Mode:"));
	gtk_box_pack_start(GTK_BOX(main_vbox), frame, FALSE, TRUE, 0);

	hbox = gtk_hbox_new(FALSE, 4);
	gtk_container_add(GTK_CONTAINER(frame), hbox);
	gtk_container_set_border_width(GTK_CONTAINER(hbox), 4);

	label = gtk_label_new(_("Default mode:"));
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, TRUE, 0);

	option_menu = gtk_option_menu_new();
	gtk_box_pack_start(GTK_BOX(hbox), option_menu, FALSE, TRUE, 0);

	menu = gtk_menu_new();
	for (i = 0; mitem_texts[i]; ++i) {
		mitem = gtk_menu_item_new_with_label(_(mitem_texts[i]));
		gtk_signal_connect(GTK_OBJECT(mitem), "activate",
				   index_view_mitem_activate_cb, NULL);
		gtk_menu_append(GTK_MENU(menu), mitem);
		gtk_widget_show(mitem);
	}

	gtk_option_menu_set_menu(GTK_OPTION_MENU(option_menu), menu);

	/* Colors frame */

	frame = gtk_frame_new(_("Colors:"));
	gtk_box_pack_start(GTK_BOX(main_vbox), frame, FALSE, TRUE, 0);

	table = gtk_table_new(3, 2, FALSE);
	gtk_container_add(GTK_CONTAINER(frame), table);
	gtk_container_set_border_width(GTK_CONTAINER(table), 4);
	gtk_table_set_row_spacings(GTK_TABLE(table), 2);
	gtk_table_set_col_spacings(GTK_TABLE(table), 8);

	for (i = 0; color_texts[i]; ++i) {
		label = gtk_label_new(_(color_texts[i]));
		gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_table_attach(GTK_TABLE(table), label,
				 0, 1, i, i + 1,
				 GTK_FILL, 0, 0, 0);
		color_picker[i] = gnome_color_picker_new();
		gtk_table_attach(GTK_TABLE(table), color_picker[i],
				 1, 2, i, i + 1,
				 GTK_EXPAND | GTK_FILL, 0, 0, 0);
		gnome_color_picker_set_d(GNOME_COLOR_PICKER(color_picker[i]),
					 (i == 0) ? 0.8 : 0.0,
					 (i == 1) ? 0.8 : 0.0,
					 (i == 2) ? 0.8 : 0.0,
					 0.0);
	}

	index_view_pref_set_color_picker_color(color_picker[COLOR_TRANSLATED],
					       "/apps/gnopo/index_view/translated_color");
	gtk_signal_connect(GTK_OBJECT(color_picker[COLOR_TRANSLATED]),
			   "color_set", index_view_color_picker_color_set_cb,
			   GINT_TO_POINTER(COLOR_TRANSLATED));
	
	index_view_pref_set_color_picker_color(color_picker[COLOR_FUZZY],
					       "/apps/gnopo/index_view/fuzzy_color");
	gtk_signal_connect(GTK_OBJECT(color_picker[COLOR_FUZZY]),
			   "color_set", index_view_color_picker_color_set_cb,
			   GINT_TO_POINTER(COLOR_FUZZY));
	
	index_view_pref_set_color_picker_color(color_picker[COLOR_UNTRANSLATED],
					       "/apps/gnopo/index_view/untranslated_color");
	gtk_signal_connect(GTK_OBJECT(color_picker[COLOR_UNTRANSLATED]),
			   "color_set", index_view_color_picker_color_set_cb,
			   GINT_TO_POINTER(COLOR_UNTRANSLATED));

	gtk_widget_show_all(main_vbox);

	return main_vbox;
}

static void
index_view_mitem_activate_cb(GtkWidget *mitem, gpointer data)
{
	g_print("hej\n");
}

static void
header_entry_changed_cb(GtkWidget *entry, gpointer data)
{
	GConfClient *client;
	GError *error = NULL;
	int entry_number = GPOINTER_TO_INT(data);
	char *str;

	client = gconf_client_get_default();
	g_return_if_fail(client != NULL);

	str = gtk_entry_get_text(GTK_ENTRY(entry));
	
	switch (entry_number) {
	case ENTRY_NAME:
		gconf_client_set_string(client,
					"/apps/gnopo/translator_name",
					str, &error);
		break;
	case ENTRY_EMAIL:
		gconf_client_set_string(client,
					"/apps/gnopo/translator_email",
					str, &error);
		break;
	default:
		g_message("%s -> header_entry_changed_cb(): Unknown entry %i\n",
			  __FILE__, entry_number);
		break;
	}

	if (error) {
		g_message("%s: header_entry_changed_cb(): %s\n",
			  __FILE__, error->message);
	} else {
		gconf_client_suggest_sync(client, NULL);
	}
}

static GtkWidget *
build_header_page(GConfClient *client)
{
	GtkWidget *main_vbox;
	GtkWidget *frame;
	GtkWidget *table;
	GtkWidget *label;
	GtkWidget *entry[5];
	static char *header_texts[] = {
		N_("Name:"),
		N_("Email:"),
		N_("Language team:"),
		N_("Content type:"),
		N_("Encoding:"),
		NULL
	};
	char *str;
	int i;

	main_vbox = gtk_vbox_new(FALSE, 4);
	
	/* Mode frame */

	frame = gtk_frame_new(_("Standard values:"));
	gtk_box_pack_start(GTK_BOX(main_vbox), frame, FALSE, TRUE, 0);

	table = gtk_table_new(5, 2, FALSE);
	gtk_container_add(GTK_CONTAINER(frame), table);
	gtk_container_set_border_width(GTK_CONTAINER(table), 4);
	gtk_table_set_row_spacings(GTK_TABLE(table), 2);
	gtk_table_set_col_spacings(GTK_TABLE(table), 8);

	for (i = 0; header_texts[i]; ++i) {
		label = gtk_label_new(_(header_texts[i]));
		gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_table_attach(GTK_TABLE(table), label,
				 0, 1, i, i + 1,
				 GTK_FILL, 0, 0, 0);
		entry[i] = gtk_entry_new();
		gtk_signal_connect(GTK_OBJECT(entry[i]), "changed",
				   header_entry_changed_cb,
				   GINT_TO_POINTER(i));
		gtk_table_attach(GTK_TABLE(table), entry[i],
				 1, 2, i, i + 1,
				 GTK_FILL|GTK_EXPAND, 0, 0, 0);
	}

	str = gconf_client_get_string(client,
				      "/apps/gnopo/translator_name", NULL);
	if (str) {
		gtk_entry_set_text(GTK_ENTRY(entry[0]), str);
		g_free(str);
	}

	str = gconf_client_get_string(client,
				      "/apps/gnopo/translator_email", NULL);
	if (str) {
		gtk_entry_set_text(GTK_ENTRY(entry[1]), str);
		g_free(str);
	}

	for (i = 0; header_texts[i]; ++i) {
		gtk_entry_set_position(GTK_ENTRY(entry[i]), 0);
	}

	gtk_widget_show_all(main_vbox);

	return main_vbox;
}
	
static GtkWidget *
build_general_settings_page(GConfClient *client)
{
	GtkWidget *main_vbox;
	GtkWidget *frame;

	main_vbox = gtk_vbox_new(FALSE, 4);

	frame = gtk_frame_new(_("Loading and saving:"));
	gtk_box_pack_start(GTK_BOX(main_vbox), frame, FALSE, TRUE, 0);

	gtk_widget_show_all(main_vbox);

	return main_vbox;
}

/* Displays a font selection dialog and, if a new font is chosen, puts
 * the font string in the supplied text entry.
 */
static void
textbox_font_button_cb(GtkWidget *button, gpointer data)
{
	GtkWidget *dialog;
	GtkWidget *fontsel;
	GtkWidget *entry;
	int button_pressed;
	char *fontname;

	g_assert(data != NULL);

	entry = GTK_WIDGET(data);

	dialog = gnome_dialog_new(_("Choose a font"),
				  GNOME_STOCK_BUTTON_OK,
				  GNOME_STOCK_BUTTON_CANCEL,
				  NULL);
	gnome_dialog_set_default(GNOME_DIALOG(dialog), 0);
	gnome_dialog_close_hides(GNOME_DIALOG(dialog), TRUE);

	fontsel = gtk_font_selection_new();
	gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dialog)->vbox), fontsel,
			   FALSE, TRUE, 0);
	gtk_widget_show(fontsel);
	
	button_pressed = gnome_dialog_run_and_close(GNOME_DIALOG(dialog));

	switch (button_pressed) {
	case 0:
		g_print("OK button was pressed.\n");
		fontname = gtk_font_selection_get_font_name(GTK_FONT_SELECTION(fontsel));
		if (fontname) {
			gtk_entry_set_text(GTK_ENTRY(entry), fontname);
		} else {
			gtk_entry_set_text(GTK_ENTRY(entry), "bonk");
		}
		gtk_entry_set_position(GTK_ENTRY(entry), 0);
		break;
	case 1:
		g_print("Cancel button was pressed.\n");
		break;
	case -1:
		g_print("No button was pressed.\n");
		break;
	default:
		g_assert_not_reached();
	}
	/* We only hid it before. */
	gtk_widget_destroy(dialog);
}

static GtkWidget *
build_textbox_page(GConfClient *client)
{
	GtkWidget *main_vbox;
	GtkWidget *frame;
	GtkWidget *hbox;
	GtkWidget *label;
	GtkWidget *entry;
	GtkWidget *button;

	main_vbox = gtk_vbox_new(FALSE, 4);

	frame = gtk_frame_new(_("Appearance:"));
	gtk_box_pack_start(GTK_BOX(main_vbox), frame, FALSE, TRUE, 0);

	hbox = gtk_hbox_new(FALSE, 4);
	gtk_container_set_border_width(GTK_CONTAINER(hbox), 4);
	gtk_container_add(GTK_CONTAINER(frame), hbox);

	label = gtk_label_new(_("Font:"));
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, TRUE, 0);

	entry = gtk_entry_new();
	gtk_box_pack_start(GTK_BOX(hbox), entry, FALSE, TRUE, 0);

	button = gtk_button_new_with_label(_("Choose font..."));
	gtk_box_pack_start(GTK_BOX(hbox), button, FALSE, TRUE, 0);
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
			   textbox_font_button_cb, entry);

	gtk_widget_show_all(main_vbox);

	return main_vbox;
}
