/* -*- Mode: c; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*-
 *
 * main.c: The main file of Gnopo.. Where it all begins.
 *
 * Copyright (C) 2000 Andreas Persenius
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Andreas Persenius <ndap@swipnet.se>
 */
#include "gnopo-window.h"
#include <gnome.h>
#include <gconf/gconf-client.h>

int init_gconf(int argc, char *argv[]);

/* Returns 0 if GConf init fails. */
int
init_gconf(int argc, char *argv[])
{
    GError *error;
    GConfClient *client;

    error = NULL;
    if (! gconf_init(argc, argv, &error)) {
	    g_message("Initialization of GConf failed: %s\n", error->message);
	    g_error_free(error);
	    return 0;
    }

    client = gconf_client_get_default();
    if (! client) {
	    g_message("Could not obtaion default GConf client.\n");
	    return 0;
    }
    
    error = NULL;
    gconf_client_add_dir(client, "/apps/gnopo",
			 GCONF_CLIENT_PRELOAD_NONE, &error);
    if (error) {
	    g_message("Failed to add GConf directory /apps/gnopo: %s\n",
		      error->message);
	    return 0;
    }
    return 1;
}

int
main(int argc, char *argv[])
{
    GtkWidget *window;
    
    bindtextdomain(PACKAGE, GNOMELOCALEDIR);
    textdomain(PACKAGE);

    gnome_init("Gnopo", VERSION, argc, argv);

    if (! init_gconf(argc, argv)) {
	    exit(1);
    }

    window = gnopo_window_new();
    gtk_widget_show(window);

    gtk_main();
    return(0);
}
