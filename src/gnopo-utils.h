/* -*- Mode: c; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*-
 *
 * gnopo-utils.h: Miscellaneous useful functions for Gnopo.
 *
 * Copyright (C) 2000 Andreas Persenius
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Andreas Persenius <ndap@swipnet.se>
 *
 */

#ifndef GNOPO_UTILS_H
#define GNOPO_UTILS_H

#include <gtk/gtk.h>

void gtk_widget_set_size_in_chars(GtkWidget *widget, int width, int height);
char *replace_newlines(const char *str);
int find_break_point(const char *string, int max);
GSList *word_wrap(const char *long_string, int line_length);
GSList *split_newlines(const char *string);

#endif /* GNOPO_UTILS_H */
