/* -*- Mode: c; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*-
 *
 * pofile-parser.y: Yacc parser for po-files.
 *
 * Copyright (C) 2000 Andreas Persenius
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Andreas Persenius <ndap@swipnet.se>
 */
%{
#include "pofile-private.h"
#include <glib.h>
#include <stdlib.h>

#define YYERROR_VERBOSE

int yylex(void);
static int yyerror(char *s);
static void pofile_parser_handle_message(void);
static void pofile_parser_handle_comment(char *comment);
static void pofile_parser_handle_msgid(char *msgid);
static void pofile_parser_handle_msgstr(char *msgstr);
static void handle_comment(char **dest, char *comment,
			   gboolean preserve_formatting);
static void handle_obsolete_comment(char *comment);
static void handle_flags(const char *comment);


extern FILE *yyin;
static PofileMessage *msg = NULL;
static GSList *msglist = NULL;
static char *obsolete_messages = NULL;
%}

%union {
    char *str;
}

%token <str> COMMENT
%token <str> STRING
%token MSGID MSGSTR

%type <str> msgid msgstr strings comment

%%
input	: /* empty */
	| input message { pofile_parser_handle_message(); }
;

message : comment msgid msgstr { }
	| msgid msgstr { }
| comment { }
;

msgid	: MSGID strings { pofile_parser_handle_msgid($2); }
;


msgstr	: MSGSTR strings { pofile_parser_handle_msgstr($2); }
;

strings	: STRING { $$ = $1;}
	| strings STRING {
	$$ = g_strconcat($1, $2, NULL);
	free($1);
	free($2);
	}
;

comment : COMMENT { pofile_parser_handle_comment($1); }
        | comment COMMENT { pofile_parser_handle_comment($2); }
;


%%

int yyerror(char *s) {
	printf("ERROR(%s)\n", s);
	return(0);
}

/**
 * pofile_parser_parse_file:
 * @fp: A file handle ready for reading. Should point at a .po file.
 *
 * Reads and parses the file pointed to by @fp.
 *
 * Returns: A GSList containing the messages in the file. There is one
 * GnopoMessage per node in the list. If no messages could be found, the
 * returned list is NULL.
 **/
GSList *
pofile_parser_parse_file(FILE *fp)
{
	GSList *list;
    
	g_assert(msglist == NULL);
	g_assert(obsolete_messages == NULL);
	g_return_val_if_fail(fp != NULL, NULL);

	yyin = fp;
	yyparse();

	list = g_slist_reverse(msglist);
	msglist = NULL;
	return(list);
}

/* Returns a string with all obsolete (#~) messages. pofile_parser_parse_file
 * must be used first. */
char *
pofile_parser_get_obsolete(void)
{
	return(obsolete_messages);
}

static void
pofile_parser_handle_message(void)
{
	g_assert(msg != NULL);
    
	if (obsolete_messages != NULL) {
		return;
	}
	
	msglist = g_slist_prepend(msglist, msg);
	msg = NULL;
}

static void
pofile_parser_handle_comment(char *comment)
{
	g_assert(comment != NULL);

	if (!msg) {
		msg = g_new0(PofileMessage, 1);
	}

	switch (comment[1]) {
	case ' ': /* Translator comments */
		handle_comment(&msg->usercom, comment, TRUE);
		break;
	case ':': /* Reference. Usually to file(s) */
		handle_comment(&msg->ref, comment, TRUE);
		break;
	case ',': /* Flag (fuzzy, c-format or no-c-format) */
		handle_flags(comment);
		break;
	case '.': /* Source code comment */
		handle_comment(&msg->autocom, comment, TRUE);
		break;
	case '~': /* obsolete entry */
		handle_obsolete_comment(comment);
		break;
	case '\0':
		break;
	default:
		g_assert_not_reached();
	}
}

static void
pofile_parser_handle_msgid(char *msgid)
{
	g_assert(msgid != NULL);
    
	if (!msg)
		msg = g_new0(PofileMessage, 1);
	else
		g_return_if_fail(msg->msgid == NULL);
    
	msg->msgid = msgid;
}

static void
pofile_parser_handle_msgstr(char *msgstr)
{
	g_assert(msgstr != NULL);

	if (!msg)
		msg = g_new0(PofileMessage, 1);
	else
		g_return_if_fail(msg->msgstr == NULL);

	msg->msgstr = msgstr;
}

/* Concatenates multi line comments to a single string. */
static void
handle_comment(char **dest, char *comment, gboolean preserve_formatting)
{
	char *str;
	int len, offset;

	len = strlen(comment);
	offset = (comment[2] == ' ') ? 3 : 2;
	g_memmove(comment, comment + offset, len - offset);
	comment[len-offset] = '\0';
    
	if (*dest) {
		if (preserve_formatting == TRUE) {
			str = g_strjoin("\n", *dest, comment, NULL);
		} else {
			str = g_strconcat(*dest, comment, NULL);
		}
		g_free(*dest);
		*dest = str;
	} else {
		*dest = g_strdup(comment);
	}
}

static void
handle_obsolete_comment(char *comment)
{
	char *str;

	if (obsolete_messages == NULL) {
		obsolete_messages = g_strconcat(comment, "\n", NULL);
		return;
	}

	/* Insert an empty line before obsolete msgid's since the
	 * parser strips them. */
	if (strstr(comment, "msgid") != NULL) {
		str = g_strconcat(obsolete_messages, "\n", comment, "\n", NULL);
	} else {
		str = g_strconcat(obsolete_messages, comment, "\n", NULL);
	}
	g_free(obsolete_messages);
	obsolete_messages = str;
}

static void
handle_flags(const char *comment)
{
	if (strstr(comment, "fuzzy") == NULL) {
		msg->fuzzy = FALSE;
	} else {
		msg->fuzzy = TRUE;
	}

	if (strstr(comment, "no-c-format") == NULL) {
		msg->no_c_format = FALSE;
		if (strstr(comment, "c-format") == NULL) {
			msg->c_format = FALSE;
		} else {
			msg->c_format = TRUE;
		}
	} else {
		msg->no_c_format = TRUE;
		msg->c_format = FALSE;
	}
}
