/* -*- Mode: c; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*-
 *
 * gnopo-utils.c: Miscellaneous useful functions for Gnopo.
 *
 * Copyright (C) 2000 Andreas Persenius
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Andreas Persenius <ndap@swipnet.se>
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include "gnopo-utils.h"
#include <string.h>

void
gtk_widget_set_size_in_chars(GtkWidget *widget, int width, int height)
{
	int x_char_width, X_char_height;

	g_return_if_fail(widget != NULL);

	x_char_width = gdk_char_width(widget->style->font, 'x');
	X_char_height = gdk_char_height(widget->style->font, 'X');

	gtk_widget_set_usize(widget,
			     (width > 0) ? width * x_char_width : -1,
			     (height > 0) ? height * X_char_height : -1);
}

/* Returns a new string with all instances of "\n" replaced with "\\n". */
char *
replace_newlines(const char *str)
{
	GString *new_str;
	char *s;
	static int i = 0;

	if (! str) {
		return NULL;
	}
	i++;
	new_str = g_string_sized_new(strlen(str));
	for (s = (char *) str; s[0] != '\0'; s++) {
		if (s[0] == '\n') {
			new_str = g_string_append(new_str, "\\n");
		} else {
			new_str = g_string_append_c(new_str, s[0]);
		}
	}

	s = new_str->str;
	g_string_free(new_str, FALSE);

	return s;
}

/**
 * find_break_point:
 * @string: A NULL terminated string to search for break point.
 * @max: Try to find a break point before this offset.
 *
 * Finds the best place to line break @string. If it finds a newline
 * (\n), it will return that immediately. Otherwise it finds the space
 * closest under @max.
 *
 * Returns: The offset in characters to the break point.
 */
int
find_break_point(const char *string, int max)
{
	int bp = 0;
	int i = 0;

	g_assert(string != NULL);
	g_assert(max > 0);

	while (string[i]) {
		if (string[i] == '\n')
			return i;
		if (string[i] == ' ')
			bp = i;
		++i;
		if (i >= max && bp != 0)
			return bp;
	}
	bp = i;
	return bp;
}

GSList *
word_wrap(const char *long_string, int line_length)
{
	GSList *lines = NULL;
	char *new_line = NULL;
	char *curpos;
	gint len;

	g_assert(long_string != NULL);
	g_assert(line_length > 0);

	curpos = (char *) long_string;
	do {
		len = find_break_point(curpos, line_length);
		if (curpos[len] == ' ')
			len++;
		new_line = g_strndup(curpos, len);
		lines = g_slist_append(lines, new_line);
		curpos = curpos + len;
		if (*curpos && *curpos == '\n')
			curpos++;
	} while (*curpos);
    
	g_assert(lines != NULL);
	return lines;
}

GSList *
split_newlines(const char *string)
{
	GSList *lines = NULL;
	char *line_begin = NULL, *s = NULL, *str = NULL;
	int len = 1;

	g_assert(string != NULL);

	line_begin = (char *) string;
	while (len && line_begin) {
		s = strstr(line_begin, "\\n");
		if (s)
			s += 2;
		else
			s = strchr(line_begin, '\0');
		len = (int) (s - line_begin);
		if (len > 0) {
			str = g_strndup(line_begin, len);
			lines = g_slist_append(lines, str);
		}
		line_begin = s;
	}
	return lines;
}
